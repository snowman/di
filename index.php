<?php
date_default_timezone_set('Europe/Prague');

use Show\DependencyInjection\Configuration;
use Show\DependencyInjection\Container;
use Show\DependencyInjection\Registry;
use Show\DependencyInjection\TokenServiceMatcher;
use Show\Service\Security;
use Show\Service\UserAuthenticator;
use Show\Service\UserProvider;

require_once __DIR__ . '/vendor/autoload.php';

$configuration = new Configuration();
$configuration->add('security.user.provider', UserProvider::class);
$configuration->add('security.user.authenticator', UserAuthenticator::class);
$configuration->add('security.user.authenticator.second', UserAuthenticator::class);
$configuration->add('security', Security::class, ['greetings' => 'Security has been fully loaded']);

$tokenServiceMatcher = new TokenServiceMatcher($configuration);

$registry = new Registry();
$container = new Container($registry, $tokenServiceMatcher);

// Services
$security = $container->getService('security');
if (!$security instanceof Security) {
    throw new LogicException('Service "security" must be an instance of Security class.');
}

echo '---------------------------------' . PHP_EOL;
$security->greet();
$security->authenticate();
echo '---------------------------------' . PHP_EOL;
