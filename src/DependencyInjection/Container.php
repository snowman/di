<?php

namespace Show\DependencyInjection;

use InvalidArgumentException;
use ReflectionClass;
use ReflectionParameter;
use Show\Cache\Cache;
use Show\DependencyInjection\Cache\Parameter;
use Show\DependencyInjection\Cache\ParameterCollection;

final readonly class Container
{
    public function __construct(
        private Registry            $registry,
        private TokenServiceMatcher $tokenServiceMatcher,
    )
    {
    }

    public function addService(string $token, object $service): void
    {
        $this->registry->addService($token, $service);
    }

    public function getService(string $token): object
    {
        if ($this->registry->hasService($token)) {
            return $this->registry->getService($token);
        }

        $service = $this->createService($token);
        $this->registry->addService($token, $service);

        return $service;
    }

    private function createService(string $token): object
    {
        $fqcn = $this->tokenServiceMatcher->match($token);

        $cache = new Cache(modifier: '+5 seconds');
        $constructorJson = $cache->get('dependency_injection.' . $token . '.constructor.parameters', function () use ($fqcn) {
            $reflection = new ReflectionClass($fqcn);
            if (!$reflection->isInstantiable()) {
                throw new InvalidArgumentException(
                    sprintf('Service "%s" is not instantiable.', $fqcn)
                );
            }

            $params = $reflection->getConstructor()?->getParameters() ?? [];
            $params = array_map(function (ReflectionParameter $reflectionParameter) {
                return new Parameter($reflectionParameter->getName(), $reflectionParameter->getType()->getName());
            }, $params);

            $paramCollection = new ParameterCollection();
            foreach ($params as $param) {
                $paramCollection->add($param);
            }

            return $paramCollection->jsonSerialize();
        });

        $collection = ParameterCollection::createFromJson($constructorJson);

        if ($collection->count() === 0) {
            return new $fqcn();
        }

        $givenParameters = $this->tokenServiceMatcher->getParamsForToken($token);
        $dependencies = $this->resolveDependencies($collection, $givenParameters);

        return new $fqcn(...$dependencies);
    }

    private function resolveDependencies(ParameterCollection $parameterCollection, array $parametersFromConfiguration = []): array
    {
        $dependencies = [];
        foreach ($parameterCollection->all() as $param) {
            $givenKeys = array_keys($parametersFromConfiguration);
            if (in_array($param->getName(), $givenKeys)) {
                $dependencies[] = $parametersFromConfiguration[$param->getName()];
                continue;
            }

            $paramType = $param->getType();
            $token = $this->tokenServiceMatcher->matchByFqcn($paramType);
            $dependencies[] = $this->getService(false === $token ? $paramType : $token);
        }

        return $dependencies;
    }
}
