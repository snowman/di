<?php

namespace Show\DependencyInjection;

class Configuration
{
    private array $services = [];
    private array $params = [];

    public function add(string $token, string $fqcn, array $params = []): void
    {
        $this->services[$token] = $fqcn;
        $this->params[$token] = $params;
    }

    public function all(): array
    {
        return $this->services;
    }

    public function get(string $token): string|null
    {
        return $this->services[$token] ?? null;
    }

    public function getParams(string $token): array
    {
        return $this->params[$token] ?? [];
    }

    public function getTokenByFqcn(string $fqcn): string|false
    {
        return array_search($fqcn, $this->services);
    }
}