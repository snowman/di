<?php

namespace Show\DependencyInjection;

use InvalidArgumentException;

class Registry
{
    private array $services = [];

    public function addService(string $token, object $service): void
    {
        if ($this->hasService($token)) {
            throw new InvalidArgumentException(
                sprintf('Service "%s" already exists.', $token)
            );
        }

        $this->services[$token] = $service;
    }

    public function hasService(string $token): bool
    {
        return isset($this->services[$token]);
    }

    public function getService(string $token): object
    {
        return $this->services[$token];
    }

    public function all(): array
    {
        return $this->services;
    }
}