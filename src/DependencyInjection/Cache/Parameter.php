<?php

namespace Show\DependencyInjection\Cache;

use JsonSerializable;

class Parameter implements JsonSerializable
{
    public function __construct(
        private readonly string $name,
        private readonly string $type,
    )
    {
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function isPrimitive(): bool
    {
        return !is_object($this->type);
    }

    public function isObject(): bool
    {
        return is_object($this->type);
    }

    public function jsonSerialize(): string
    {
        return json_encode([
            'name' => $this->name,
            'type' => $this->type,
        ]);
    }

    public static function createFromJson(string $json): self
    {
        $data = json_decode($json, true);

        return new self($data['name'], $data['type']);
    }
}
