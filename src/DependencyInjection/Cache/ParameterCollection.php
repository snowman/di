<?php

namespace Show\DependencyInjection\Cache;

use InvalidArgumentException;
use JsonSerializable;

class ParameterCollection implements JsonSerializable
{
    private array $parameters = [];

    public function add(Parameter $parameter): void
    {
        $this->parameters[] = $parameter;
    }

    public function get(string $name): Parameter
    {
        foreach ($this->parameters as $parameter) {
            if ($parameter->getName() === $name) {
                return $parameter;
            }
        }

        throw new InvalidArgumentException(sprintf('Parameter with name %s not found.', $name));
    }

    public function has(string $name): bool
    {
        foreach ($this->parameters as $parameter) {
            if ($parameter->getName() === $name) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return Parameter[]
     */
    public function all(): array
    {
        return $this->parameters;
    }

    public function count(): int
    {
        return count($this->parameters);
    }

    public function jsonSerialize(): string
    {
        return json_encode($this->parameters);
    }

    public static function createFromJson(string $json): self
    {
        $data = json_decode($json, true);

        $collection = new self();
        foreach ($data as $parameter) {
            $collection->add(Parameter::createFromJson($parameter));
        }

        return $collection;
    }
}