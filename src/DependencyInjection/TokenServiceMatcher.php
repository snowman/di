<?php

namespace Show\DependencyInjection;

use InvalidArgumentException;

class TokenServiceMatcher
{
    public function __construct(
        private readonly Configuration $configuration,
    )
    {
    }

    public function match(string $token): string
    {
        $fqcn = $this->findInConfiguration($token);
        if ($fqcn !== null) {
            return $fqcn;
        }

        if ($this->existsFqcn($token)) {
            return $token;
        }

        throw new InvalidArgumentException(sprintf('Service "%s" not found.', $token));
    }

    public function matchByFqcn(string $fqcn): string|false
    {
        return $this->configuration->getTokenByFqcn($fqcn);
    }

    public function getParamsForToken(string $token): array
    {
        return $this->configuration->getParams($token);
    }

    public function findInConfiguration(string $token): string|null
    {
        $serviceClassName = $this->configuration->get($token) ?? null;
        if ($serviceClassName !== null && !class_exists($serviceClassName)) {
            throw new InvalidArgumentException(sprintf('Service class "%s" not found.', $serviceClassName));
        }

        return $serviceClassName;
    }

    public function existsFqcn(string $fqcn): bool
    {
        return class_exists($fqcn);
    }
}
