<?php

namespace Show\Service;

class Security
{
    public function __construct(
        private UserAuthenticator $userAuthenticator,
        private string            $greetings,
    )
    {
    }

    public function greet(): void
    {
        echo $this->greetings . PHP_EOL;
    }

    public function authenticate(): void
    {
        echo 'Security::authenticate() called.' . PHP_EOL;
        $this->userAuthenticator->authenticate();
    }
}
