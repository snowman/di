<?php

namespace Show\Service;

class UserAuthenticator
{
    public function __construct(
        private UserProvider $userProvider,
    )
    {
        echo 'UserAuthenticator created.' . PHP_EOL;
    }

    public function authenticate(): void
    {
        echo 'UserAuthenticator::authenticate() called.' . PHP_EOL;
    }
}
