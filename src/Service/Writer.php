<?php

namespace Show\Service;

class Writer
{
    public function write(string $message): void
    {
        echo $message . PHP_EOL;
    }
}