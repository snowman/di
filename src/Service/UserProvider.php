<?php

namespace Show\Service;

class UserProvider
{
    public function __construct(
        private readonly Writer $writer,
    )
    {
        $this->writer->write('UserProvider created.');
    }
}
