<?php

namespace Show\Cache;

use DateTimeImmutable;

class Cache
{
    public function __construct(
        private string $directory = __DIR__ . '/../../var/cache',
        private string $modifier = '+10 second',
    )
    {
        if (!is_dir($this->directory)) {
            mkdir($this->directory, recursive: true);
        }
    }

    public function writeCache(string $name, string $json): void
    {
        file_put_contents($this->directory . '/' . $name . '.json', $json);
    }

    public function get(string $name, callable $callback): string
    {
        $file = $this->directory . '/' . $name . '.json';
        if (!file_exists($file)) {
            echo 'Cache miss: ' . $name . PHP_EOL;

            $json = $callback();
            $array = json_decode($json, true);
            $array['expired'] = (new DateTimeImmutable())->modify($this->modifier)->format('Y-m-d H:i:s');

            $json = json_encode($array);
            $this->writeCache($name, $json);

            $array = json_decode($json, true);
            unset($array['expired']);
            return json_encode($array);
        }

        $json = file_get_contents($file);
        $array = json_decode($json, true);
        $expired = DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $array['expired']) < new DateTimeImmutable();

        if ($expired) {
            echo 'Cache expired: ' . $name . PHP_EOL;

            $json = $callback();
            $array = json_decode($json, true);
            $array['expired'] = (new DateTimeImmutable())->modify($this->modifier)->format('Y-m-d H:i:s');

            $json = json_encode($array);
            $this->writeCache($name, $json);

            $array = json_decode($json, true);
            unset($array['expired']);
            return json_encode($array);
        }

        echo 'Cache hit: ' . $name . PHP_EOL;

        $array = json_decode($json, true);
        unset($array['expired']);
        return json_encode($array);
    }
}
